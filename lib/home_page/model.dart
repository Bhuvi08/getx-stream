// To parse this JSON data, do
//
//     final response = responseFromJson(jsonString);

import 'dart:convert';

KarikalaResponse responseFromJson(String str) => KarikalaResponse.fromJson(json.decode(str));

String responseToJson(KarikalaResponse data) => json.encode(data.toJson());

class KarikalaResponse {
  KarikalaResponse({
    this.status,
    this.message,
    this.response,
  });

  String status;
  String message;
  ResponseClass response;

  factory KarikalaResponse.fromJson(Map<String, dynamic> json) => KarikalaResponse(
    status: json["status"],
    message: json["message"],
    response: ResponseClass.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "response": response.toJson(),
  };
}

class ResponseClass {
  ResponseClass({
    this.stp1,
    this.stp2,
    this.motor,
    this.level,
    this.flowMeter,
    this.qa,
    this.motorDetails,
    this.levelDetails,
    this.flowDetails,
    this.qaDetails,
    this.owner,
    this.mqtt,
  });

  List<dynamic> stp1;
  List<dynamic> stp2;
  int motor;
  int level;
  int flowMeter;
  int qa;
  List<dynamic> motorDetails;
  List<LevelDetail> levelDetails;
  List<dynamic> flowDetails;
  List<QaDetail> qaDetails;
  Owner owner;
  Mqtt mqtt;

  factory ResponseClass.fromJson(Map<String, dynamic> json) => ResponseClass(
    stp1: List<dynamic>.from(json["stp1"].map((x) => x)),
    stp2: List<dynamic>.from(json["stp2"].map((x) => x)),
    motor: json["motor"],
    level: json["level"],
    flowMeter: json["flow_meter"],
    qa: json["qa"],
    motorDetails: List<dynamic>.from(json["motor_details"].map((x) => x)),
    levelDetails: List<LevelDetail>.from(json["level_details"].map((x) => LevelDetail.fromJson(x))),
    flowDetails: List<dynamic>.from(json["flow_details"].map((x) => x)),
    qaDetails: List<QaDetail>.from(json["qa_details"].map((x) => QaDetail.fromJson(x))),
    owner: Owner.fromJson(json["owner"]),
    mqtt: Mqtt.fromJson(json["mqtt"]),
  );

  Map<String, dynamic> toJson() => {
    "stp1": List<dynamic>.from(stp1.map((x) => x)),
    "stp2": List<dynamic>.from(stp2.map((x) => x)),
    "motor": motor,
    "level": level,
    "flow_meter": flowMeter,
    "qa": qa,
    "motor_details": List<dynamic>.from(motorDetails.map((x) => x)),
    "level_details": List<dynamic>.from(levelDetails.map((x) => x.toJson())),
    "flow_details": List<dynamic>.from(flowDetails.map((x) => x)),
    "qa_details": List<dynamic>.from(qaDetails.map((x) => x.toJson())),
    "owner": owner.toJson(),
    "mqtt": mqtt.toJson(),
  };
}

class LevelDetail {
  LevelDetail({
    this.id,
    this.tankName,
    this.height,
    this.totalCapacity,
    this.currentCapacity,
    this.currentPercentage,
    this.tankType,
    this.unit,
    this.weighBridge,
    this.label,
  });

  String id;
  String tankName;
  String height;
  String totalCapacity;
  String currentCapacity;
  String currentPercentage;
  String tankType;
  String unit;
  String weighBridge;
  String label;

  factory LevelDetail.fromJson(Map<String, dynamic> json) => LevelDetail(
    id: json["id"],
    tankName: json["tank_name"],
    height: json["height"],
    totalCapacity: json["total_capacity"],
    currentCapacity: json["current_capacity"],
    currentPercentage: json["current_percentage"],
    tankType: json["tank_type"],
    unit: json["unit"],
    weighBridge: json["weigh_bridge"],
    label: json["label"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "tank_name": tankName,
    "height": height,
    "total_capacity": totalCapacity,
    "current_capacity": currentCapacity,
    "current_percentage": currentPercentage,
    "tank_type": tankType,
    "unit": unit,
    "weigh_bridge": weighBridge,
    "label": label,
  };
}

class Mqtt {
  Mqtt({
    this.host,
    this.port,
    this.username,
    this.password,
    this.topic,
  });

  String host;
  String port;
  String username;
  String password;
  String topic;

  factory Mqtt.fromJson(Map<String, dynamic> json) => Mqtt(
    host: json["host"],
    port: json["port"],
    username: json["username"],
    password: json["password"],
    topic: json["topic"],
  );

  Map<String, dynamic> toJson() => {
    "host": host,
    "port": port,
    "username": username,
    "password": password,
    "topic": topic,
  };
}

class Owner {
  Owner({
    this.ownerName,
    this.dialCode,
    this.phoneNumber,
    this.email,
    this.apartmentName,
    this.address,
    this.logo,
  });

  String ownerName;
  String dialCode;
  String phoneNumber;
  String email;
  String apartmentName;
  String address;
  String logo;

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
    ownerName: json["owner_name"],
    dialCode: json["dial_code"],
    phoneNumber: json["phone_number"],
    email: json["email"],
    apartmentName: json["apartment_name"],
    address: json["address"],
    logo: json["logo"],
  );

  Map<String, dynamic> toJson() => {
    "owner_name": ownerName,
    "dial_code": dialCode,
    "phone_number": phoneNumber,
    "email": email,
    "apartment_name": apartmentName,
    "address": address,
    "logo": logo,
  };
}

class QaDetail {
  QaDetail({
    this.id,
    this.name,
    this.currrentValue,
    this.currentLabel,
  });

  String id;
  String name;
  String currrentValue;
  String currentLabel;

  factory QaDetail.fromJson(Map<String, dynamic> json) => QaDetail(
    id: json["id"],
    name: json["name"],
    currrentValue: json["currrent_value"],
    currentLabel: json["current_label"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "currrent_value": currrentValue,
    "current_label": currentLabel,
  };
}
