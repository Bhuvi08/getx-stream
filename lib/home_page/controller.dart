import 'dart:async';
import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'model.dart';

class LevelController extends GetxController {
  Client client =Client();
  RxList<KarikalaResponse> levelList = <KarikalaResponse>[].obs;
  StreamController<KarikalaResponse> userController;

  @override
  void onInit() {
    streamdata();
    super.onInit();
  }

 Future streamdata() async{
    var response = await client.post(Uri.parse(Url),body: {
      'params':'123456789'
    });
    if(response.statusCode !=200){
      return Future.error(response.statusCode);
    }
    else{
   var jsonResponse = json.decode(response.body);
      KarikalaResponse enddata= KarikalaResponse.fromJson(jsonResponse);
      return enddata;
    }
  }

  Future loadDetails() async {
    LevelController().streamdata().then((res) async{
      userController.add(res);
      return res;
    });
  }

  // Future handleRefresh() async {
  //   LevelController().streamdata().then((res) async {
  //     userController.add(res);
  //     return userController;
  //   });
  // }

}
