import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_final/home_page/model.dart';
import 'controller.dart';

class LevelView extends GetView<LevelController> {
  final controllerData = LevelController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Level Data",style: TextStyle(color: Colors.black,fontSize: 16),),
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          child: GetBuilder<LevelController>(
            initState: (e){
              controllerData.userController = new StreamController();
              Timer.periodic(Duration(seconds: 1), (_) =>controllerData.loadDetails());
            },
            dispose: (e){
              controllerData.userController.close();
            },
            builder: (controller) {
              return  StreamBuilder(
                  stream:  controllerData.userController.stream,
                  builder: (BuildContext context, AsyncSnapshot<KarikalaResponse> snapshot) {
                    if(snapshot.hasData){
                      return GridView.count(
                          crossAxisCount: 2,
                          shrinkWrap: true,
                          mainAxisSpacing: 8,
                          crossAxisSpacing: 5,
                          children:
                          List.generate(snapshot.data.response.levelDetails.length, (index) {
                            if(snapshot.data.response.levelDetails.length==null){
                              return CircularProgressIndicator();
                            }else{
                              return Container(
                                  margin: EdgeInsets.all(5),
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 2.0,
                                          spreadRadius: 0.0,
                                          offset: Offset(
                                              2.0, 2.0), // shadow direction: bottom right
                                        )
                                      ]),
                                  child:Column(
                                    children: [
                                      Text(
                                        snapshot.data.response.levelDetails[index].label,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: 10,),
                                      Text(
                                        snapshot.data.response.levelDetails[index].currentCapacity,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: 10,),
                                      Text(
                                        snapshot.data.response.levelDetails[index].totalCapacity,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ));}},));
                    }else{
                      return CircularProgressIndicator();
                    }
                });
            },
          ),
        ),
      ),
    );
  }
}